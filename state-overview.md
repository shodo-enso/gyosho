```mermaid
stateDiagram
  state fork_state <<fork>>
      [*] -->  fork_state
      fork_state --> EastWestPedestrian
      fork_state -->  EastWestTraffic
      state EastWestTraffic {
        [*] --> Green
        Green --> Yellow : Signal Received or Timeout
        Yellow --> Red
        Red --> [*]
      }
      state EastWestPedestrian {
        [*] --> Green
        Green --> BlinkingGreen : Signal Received or Timeout
        BlinkingGreen --> Red
        Red --> [*]
      }
  state join_state <<join>>
      EastWestTraffic --> join_state
      EastWestPedestrian --> join_state
      
      join_state --> SouthNorthSignals
      join_state --> EastWestSignals
      
  state SouthNorthSignals {
      [*] --> PedestrianDetected
      --
      [*] --> BusDetected
      --
      [*] --> CarDetected
  }

  state EastWestSignals {
      [*] --> PedestrianDetected
      --
      [*] --> BusDetected
      --
      [*] --> CarDetected
  }
  
    state "South-North Signal Received" as SouthNorthSignal
    state "East-West Signal Received" as EastWestSignal

    SouthNorthSignals --> SouthNorthSignal
    EastWestSignals --> EastWestSignal

    state fork2 <<fork>>
    EastWestSignal --> fork2
    SouthNorthSignal --> fork_state

    


  
      
      fork2 --> SouthNorthPedestrian
      fork2 -->  SouthNorthTraffic
      state SouthNorthTraffic {
        [*] --> Green
        Green --> Yellow : Signal Received
        Yellow --> Red
        Red --> [*]
      }
      state SouthNorthPedestrian {
        [*] --> Green
        Green --> BlinkingGreen : Signal Received
        BlinkingGreen --> Red
        Red --> [*]
      }
  state join2 <<join>>
      SouthNorthTraffic --> join2
      SouthNorthPedestrian --> join2
      join2 --> join_state      
					
  
```